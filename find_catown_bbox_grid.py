#!/usr/bin/env python

import shapefile, os, numpy
import pickle, gzip, shutil, titlecase
import selenium_redfin_demo, time
from selenium.webdriver.common.keys import Keys

def _get_bbox_records():
    filename = os.path.join( os.path.dirname(__file__),
                             'input_data/California_2011/bbox_towns.pkl.gz')
    if not os.path.isfile( filename ):
        sf =  shapefile.Reader(os.path.join( os.path.dirname(__file__), 
                                             'input_data/California_2011/tl_2011_06_place.shp' ) )
        records = sf.records()
        bbox_dict = {}
        for idx, record in enumerate(records):
            name_of_town = record[4]
            shapeOfTown = sf.shape(idx)
            bbox_dict[name_of_town] = numpy.array([ val for val in shapeOfTown.bbox ])
        pickle.dump( bbox_dict, gzip.open(filename, 'w'))
        return bbox_dict
    else:
        return pickle.load( gzip.open(filename, 'r'))
                        
_bbox_dict = _get_bbox_records()

def find_badtown_info(town_name, driver):
    driver.get('https://www.redfin.com/')
    elem = driver.find_element_by_xpath('//*[@id="searchInputNode"]/div/form/div/div/input[1]')
    elem.clear()
    elem.send_keys('%s, CA' % town_name)
    elems = driver.find_elements_by_xpath('//*[@id="searchInputNode"]/div/form/div/div/input[2]')
    if len(elems) == 1:
        max(elems).click()
        elem = driver.find_element_by_xpath('//*[@id="searchInputNode"]/div/form/div/div/input[1]')
        elem.send_keys('%s, CA' % town_name)        
    elem.send_keys(Keys.RETURN)
    #
    ## now look for bad text string
    elems_bad = driver.find_elements_by_xpath('//*[@id="redfin_common_elements_Dialog_0"]/div/div/div[2]/h3')
    assert(len(elems_bad) == 1)
    tname = ''.join(town_name.split()).lower()
    time.sleep(5.0) # sleepy sleepy :)
    for place_elem in driver.find_elements_by_class_name('place'):
        name =  ''.join(place_elem.text.split()).lower()
        if name == tname:
            place_elem.click()
            time.sleep(5.0)
            return driver.current_url
    return None
    


def push_baseurl_for_catown(town_name, driver):
    driver.get('https://www.redfin.com/')
    elem = driver.find_element_by_xpath('//*[@id="searchInputNode"]/div/form/div/div/input[1]')
    elem.clear()
    elem.send_keys('%s, CA' % town_name)
    elem.send_keys(Keys.RETURN)

def push_baseurl_for_catown_2(town_name, driver):
    driver.get('https://www.redfin.com/')
    elem = driver.find_element_by_xpath('//*[@id="searchInputNode"]/div/form/div/div/input[1]')
    elem.clear()
    elem.send_keys('%s, CA' % town_name)
    elem.send_keys(Keys.RETURN)
    time.sleep(1.5)
    

def process_all_urls():
    townnames = sorted(set(_bbox_dict.keys()))
    temp_dir, driver = selenium_redfin_demo.create_standard_chrome_driver()
    driver.implicitly_wait(5.0)
    time0 = time.time()
    allURLs = {}
    for idx, name in enumerate(townnames):
        try:
            push_baseurl_for_catown(name, driver)
            time.sleep(5.0)
            allURLs[name] = driver.current_url # have to get URL AFTERWARDS
            print name, allURLs[name]
        except Exception:
            pass
        print 'processed town %s, %d / %d, in %0.3f seconds.' % (
            name, idx + 1, len(townnames), time.time() - time0 )
    
    pickle.dump(allURLs, gzip.open('allTowns.pkl.gz', 'w'))

    #
    ## cleanup
    shutil.rmtree( temp_dir )
    driver.close()
    

"""
Divides a town into lat and long searches in units of 0.025 degrees
"""
def find_urls_for_town_with_name(name_of_town):
    assert( name_of_town in _bbox_dict )
    bbox = _bbox_dict[ name_of_town ]
    
    width_lat = int( bbox[-1] / 0.025 + 1 ) - int( bbox[1] / 0.025 )
    width_lng = int( bbox[-2] / 0.025 ) - int( bbox[0] / 0.025 - 1)
    
    start_lng_idx = int( bbox[0] / 0.025 - 1)
    end_lng_idx = int( bbox[-2] / 0.025 )
    start_lat_idx = int( bbox[1] / 0.025 )
    end_lat_idx = int( bbox[-1] / 0.025 + 1 )
    
    subURLs = []
    for idx in xrange(start_lng_idx, end_lng_idx+1):
        lng = idx * 0.025
        for jdx in xrange(start_lat_idx, end_lat_idx+1):
            lat = jdx * 0.025
            subURL = 'https://www.redfin.com/city/17151/CA/San-Francisco/real-estate#!sf=&sold_within_days=365&v=8&sst=&lat=%0.3f&long=%0.3f&zoomLevel=15&market=sanfrancisco' % ( lat, lng )
            subURLs.append(subURL)
    return subURLs

if __name__=='__main__':
    print len(_bbox_dict)
    #for url in find_urls_for_town_with_name('Livermore'):
    #    print url
