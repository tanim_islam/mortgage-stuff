#!/usr/bin/env python

import os, glob, sys, datetime, numpy
from optparse import OptionParser
import lxml.html, requests, pickle, gzip
import titlecase
from dateutil.relativedelta import relativedelta
from calendar import monthrange

_default_pmi_rate_percent = 0.35
_default_add_rate_lpmi = 0.25

def nowdate():
    dt = datetime.datetime.now()
    return datetime.date( dt.year, dt.month, dt.day )

def _get_table_loanrates():
    def is_month_row(tr_elem):
        if len(tr_elem.keys()) != 0:
            return False
        td_elems = list( tr_elem.iter('td'))
        if len(td_elems) == 0:
            return False
        txt = td_elems[0].text_content().strip()
        return txt in ( 'January', 'February', 'March', 'April', 
                        'May', 'June', 'July', 'August',
                        'September', 'October', 'November', 'December')
        
    def is_year_elem(th_elem):
        txt = th_elem.text_content().strip()
        if len(txt) == 0:
            return False
        try:
            val = int( txt )
            return True
        except ValueError:
            try:          
                val = int( txt[:-1] )
                return True
            except ValueError:
                return False
        
    def get_year_elem(year_elem):
        txt = year_elem.text_content().strip()
        try:
            val = int( txt )
        except ValueError:
            val = int( txt[:-1] )
        return val

    def process_data(tree, table_elems):
        data = {}
        date_now = datetime.date.today()
        date_first = datetime.date( date_now.year, date_now.month, 1 )
        for table_elem in table_elems:
            years = [ get_year_elem( year_elem ) for year_elem in
                      filter(is_year_elem, table_elem.iter('th')) ]
            month_rows = filter(is_month_row, table_elem.iter('tr'))
            for month_row in month_rows:
                td_elems = list( month_row.iter('td'))
                month = td_elems[0].text_content().strip()
                rate_strings = [ td_elem.text_content() for 
                                 td_elem in td_elems[1::2] ]
                rates = []
                for rate_string in rate_strings:
                    try:
                        val = float( rate_string.strip() )
                        rates.append(val)
                    except ValueError:
                        rates.append(-1)
                data.setdefault(month, {})
                for rate, year in zip(rates, years):
                    date_here = datetime.datetime.strptime('%s 1, %d' % ( month, year ),
                                                           '%B %d, %Y').date()
                    if date_here < date_first:
                        data[month][year] = rate
        
        datetime_first_month = datetime.datetime.combine( date_first, datetime.datetime.min.time())
        date_first_month_last = ( datetime_first_month - relativedelta( months = 1 )).date()
        max_date = max( get_all_dates( data ) )
        mon_label = max_date.strftime('%B')
        year_label = int( max_date.strftime('%Y' ) )
        intrst = data[ mon_label ][year_label]
        mon_now_label = date_now.strftime('%B')
        year_now_label = int( date_now.strftime('%Y') )
        data.setdefault( mon_now_label, {})
        data[mon_now_label][year_now_label] = intrst
        return data

    def get_all_dates(data):
        all_dates = []
        for mon in data:
            for year in data[mon]:
                all_dates.append( datetime.datetime.strptime( '%s 1, %d' % ( mon, year ), '%B %d, %Y').date() )
        return all_dates
    
    filename = os.path.join( os.path.dirname(__file__),
                             'input_data',
                             '30year_fha_rates.pkl.gz' )
    if not os.path.exists( filename ):
        tree = lxml.html.fromstring( requests.get('http://www.freddiemac.com/pmms/pmms30.htm').text )
        table_elems = filter(lambda elem: 'class' in elem.keys()
                             and elem.get('class') == 'table1 zebra',
                             tree.iter('table'))
        data = process_data(tree, table_elems)
        pickle.dump( data, gzip.open( filename, 'w' ) )
    else:
        data = pickle.load( gzip.open( filename, 'r' ) )
        date = datetime.datetime.today()
        date_first_month = datetime.date( date.year, date.month, 1)
        max_date = max( get_all_dates( data ) )
        if max_date != date_first_month:
            tree = lxml.html.fromstring( requests.get('http://www.freddiemac.com/pmms/pmms30.htm').text )
            table_elems = filter(lambda elem: 'class' in elem.keys()
                                 and elem.get('class') == 'table1 zebra',
                                 tree.iter('table'))
            data = process_data(tree, table_elems)
            max_date = max( get_all_dates( data ) )
            pickle.dump( data, gzip.open( filename, 'w' ) )
        
    return data

def _read_redfin_databases():
    maindir = os.path.join( os.path.dirname(__file__), 'input_data')
    input_tuples = reduce(lambda x, y: x + y, 
                          [ process_town_database( open(filename, 'r').readlines(), 'Livermore' ) for
                            filename in 
                            ( os.path.join( maindir, 'query_springtown_homes.csv'),
                              os.path.join( maindir, 'query_eastlivermore_homes.csv') ) ] )
    return input_tuples

def _read_Livermore_database():
    return process_town_database( pickle.load( gzip.open(os.path.join(
        os.path.dirname(__file__),
        'test_data', 
        'Livermore_2015_03_15.pkl.gz'), 'r')),
                                  'Livermore')
                                               

def process_town_database(all_lines, townname):
    input_tuples = []
    for line in all_lines[1:]:
        elems = [ tok.strip() for tok in line.strip().split(',') ]
        if len(elems) < 24:
            continue
        if len(elems[23]) == 0: # no price of home
            continue
        try:
            if len(elems[10].strip()) == 0:
                continue
            if elems[3].strip() != townname:
                continue
            price_sold = int( float( elems[23] ) )
            date_sold = datetime.datetime.strptime(elems[22], '%Y-%m-%d').date()
            square_foot = int( float( elems[10] ) )
            if square_foot >= 100000:
                continue
            address = titlecase.titlecase( ', '.join(elems[2:4]).lower() )
            address = ', '.join([ address, elems[4] ])
            address = ' '.join([ address, elems[5] ])
            try:
                latlng = float( elems[-3]), float( elems[-2] )
            except ValueError:
                latlng = None
            input_tuples.append(( square_foot, date_sold, price_sold, address, latlng ))
        except ValueError:
            pass
    return input_tuples

_30year_fha_dict = _get_table_loanrates()
_initial_livermore_homes = _read_Livermore_database()

def get_mortgage_rate_fha(date):
    mon_label = date.strftime('%B')
    year_label = date.year
    date_plus_1month = datetime.date( date.year, date.month, 1) + relativedelta(months = 1)
    mon_plus_label = date_plus_1month.strftime('%B')
    year_plus_label = date_plus_1month.year
    interest_start = _30year_fha_dict[mon_label][year_label]
    if mon_plus_label not in _30year_fha_dict:
        interest_end = interest_start
    else:
        if year_plus_label not in _30year_fha_dict[mon_plus_label]:
            interest_end = interest_start
        else:
            interest_end = _30year_fha_dict[mon_plus_label][year_plus_label]
    mon, numdays = monthrange( year_label, date.month )
    ratio = ( date.day - 1.0 ) / ( numdays + 1 )
    interest_rate = interest_end * ratio + interest_start * ( 1 - ratio )
    return interest_rate
        
class Mortgage(object):
    #self.interest_rate = 5.0
    #self.total_value = 500000
    #self.percent_down = 10.0
    #self.insurance_rate = 0.35
    #self.property_tax = 1.25
    #self.term_in_years = 30
    #
    """
    This formula gives an estimated monthly total
    cost of all payments that go into the house.
    The home insurance and property tax are also 
    amortized into monthly payments.
    """
    def __init__(self, total_value = 500000,
                 interest_rate = 5.0,
                 percent_down = 10.0,
                 insurance_rate = 0.35,
                 property_tax = 1.25,
                 term_in_years = 30,
                 lpmi_on = True,
                 monthly_hoa_fees = 0,
                 date = datetime.date.today() ):
        
        self.total_value = total_value
        self.interest_rate = interest_rate
        self.percent_down = percent_down
        self.insurance_rate = insurance_rate
        self.property_tax = property_tax
        self.term_in_years = term_in_years
        self.monthly_hoa_fees = monthly_hoa_fees
        self.lpmi_on = lpmi_on
        self.date = date

        # necessary information
        self.principal = (1.0 - self.percent_down / 100.0) * self.total_value
        self.monthly_insurance = self.total_value * self.insurance_rate / 100.0 / 12.0
        self.monthly_property_tax = self.total_value / 100.0 / 12.0
        
        if self.lpmi_on and self.percent_down < 20.0:
            self.interest_rate = self.interest_rate + _default_add_rate_lpmi
            self.monthly_pmi = 0.0
        else:
            self.monthly_pmi = self.total_value * _default_pmi_rate_percent / 100.0 / 12.0
        
        # I get the formula, for total monthly payments on the loan
        # from http://en.wikipedia.org/wiki/Mortgage_calculator
        rate_per_month = self.interest_rate / 12.0 / 100.0
        loan_in_months = self.term_in_years * 12
        self.monthly_mortgage = self.principal * rate_per_month * \
                                (1 + rate_per_month) ** loan_in_months / \
                                ( (1 + rate_per_month) ** loan_in_months - 1 )
        
        # total monthly payment
        self.total_monthly = self.monthly_mortgage + self.monthly_insurance + \
                             self.monthly_property_tax + self.monthly_pmi + \
                             self.monthly_hoa_fees
        
        # mortgage interest amount by month
        self.mortgage_amount_schedule = { mon : rate_per_month * self.principal * ( ( 1 + rate_per_month) ** loan_in_months - 
                                                                                    ( 1 + rate_per_month) ** (mon - 1) ) / 
                                          ( ( 1+ rate_per_month) ** loan_in_months - 1 ) for mon in xrange(1, loan_in_months + 1) }
        
    def recalculate_payments(self, new_value, percent_down = 10.0, interest_rate = 5.0):
        self.total_value = new_value
        self.percent_down = percent_down
        self.interest_rate = interest_rate
        #
        # necessary information
        self.principal = (1.0 - self.percent_down / 100.0) * self.total_value
        self.monthly_insurance = self.total_value * self.insurance_rate / 100.0 / 12.0
        self.monthly_property_tax = self.total_value / 100.0 / 12.0
        #
        if self.lpmi_on and self.percent_down < 20.0:
            self.interest_rate = self.interest_rate + _default_add_rate_lpmi
            self.monthly_pmi = 0.0
        else:
            self.monthly_pmi = self.total_value * _default_pmi_rate_percent / 100.0 / 12.0
            
        # I get the formula, for total monthly payments on the loan
        # from http://en.wikipedia.org/wiki/Mortgage_calculator
        rate_per_month = self.interest_rate / 12.0 / 100.0
        loan_in_months = self.term_in_years * 12
        self.monthly_mortgage = self.principal * rate_per_month * \
                                (1 + rate_per_month) ** loan_in_months / \
                                ( (1 + rate_per_month) ** loan_in_months - 1 )
        
        # total monthly payment
        self.total_monthly = self.monthly_mortgage + self.monthly_insurance + \
                             self.monthly_property_tax + self.monthly_pmi + \
                             self.monthly_hoa_fees
        
        # mortgage interest amount by month
        self.mortgage_amount_schedule = { mon : rate_per_month * self.principal * ( ( 1 + rate_per_month) ** loan_in_months - 
                                                                                    ( 1 + rate_per_month) ** (mon - 1) ) / 
                                          ( ( 1+ rate_per_month) ** loan_in_months - 1 ) for mon in xrange(1, loan_in_months + 1) }
        
        return self.total_monthly

    def get_interest_amount(self, yearno = 1):
        if yearno >= 1 and yearno <= self.term_in_years:
            return numpy.sum([ self.mortgage_amount_schedule[mon] for mon in xrange(1 + 12 * (yearno - 1), 1 + 12 * yearno ) ])
        else:
            return 0.0
    
    def get_interest_amount_firstmonths(self, startmonth, endmonth):
        assert(startmonth >= 1)
        assert(endmonth >= 1)
        assert(endmonth >= startmonth)
        return numpy.sum([ self.mortgage_amount_schedule[mon] for mon in xrange(startmonth, endmonth+1) ])        

    def get_start_date(self):
        date_start = datetime.date( self.date.year, self.date.month, 1) + relativedelta(months = 2)
        return date_start

    
if __name__=='__main__':
    parser = OptionParser()
    parser.add_option('--value', dest='value', type=float, action='store', default = 500000,
                      help = 'Total value of the home. Default is 500k.')
    parser.add_option('--interest', dest='interest', type=float, action='store', default = 5.0,
                      help = 'Interest rate on loan. Default is 5.0%.')
    parser.add_option('--insurancerate', dest='insurance_rate', type=float, action='store', default = 0.35,
                      help = 'Property insurance rate on the home. Default is 0.35%.')
    parser.add_option('--percentdown', dest='percent_down', type=float, action='store',
                      default = 20.0, help = 'Percentage deposit on value of home. Default is 20%.')
    parser.add_option('--propertytax', dest = 'property_tax', type=float, action='store', default = 1.25,
                      help = 'Property tax rate. Default is 1.25%.')
    parser.add_option('--term', dest='term_in_years', action='store', type=int, default = 30,
                      help = 'Term of loan in years. Default is 30 years.')
    parser.add_option('--lpmi', dest='lpmi', action='store_true', default = False,
                      help = "If chosen, add lender's private mortgage insurance to any loans in which "+
                      "less than 20% has been put down.")
    parser.add_option('--hoa', dest='hoa', action='store', type=float, default = 0,
                      help = 'Monthly HOA fees. Default is $0/month.')
    opts, args = parser.parse_args()
    mg = Mortgage( total_value = opts.value,
                   percent_down = opts.percent_down,
                   interest_rate = opts.interest,
                   insurance_rate = opts.insurance_rate,
                   property_tax = opts.property_tax,
                   term_in_years = opts.term_in_years,
                   lpmi_on = opts.lpmi,
                   monthly_hoa_fees = opts.hoa)
    print mg.total_monthly
