#!/usr/bin/env python

import numpy, datetime
import mortgage_calc
from dateutil.relativedelta import relativedelta

"""
these tax rates are for total federal and CA income, filing jointly. I got this information from irs.gov and the CA FTB.
This information is for filing in FY 2013.
"""
_marriage_deduction = 12200
_standard_federal_exemptions = 2 * 3950 # add extra 3900 for each child
_standard_california_deduction = 7812
_standard_california_marriage_credit = 212

"""
this table is taken from https://www.ftb.ca.gov/forms/2013_California_Tax_Rates_and_Exemptions.shtml for married filing jointly
"""
_minmax_rate_ca_dict = [ (0, 15164, 0.01),
                         (15165, 35952, 0.02),
                         (35953, 56742, 0.04),
                         (56743, 78768, 0.06),
                         (78769, 99548, 0.08),
                         (99549, 508500, 0.093),
                         (508501, 610200, 0.103),
                         (610201, 1017000, 0.113),
                         (1017001, 1e12, 0.123) ]

"""
table for joint married filing is taken from http://www.forbes.com/sites/kellyphillipserb/2013/01/15/irs-announces-2013-tax-rates-standard-deduction-amounts-and-more/
"""
_minmax_rate_federal_joint_dict = [ ( 0, 17850, 0.1),
                                    ( 17851, 72500, 0.15),
                                    ( 72501, 125450, 0.25),
                                    ( 125451, 203150, 0.28),
                                    ( 203151, 398350, 0.33),
                                    ( 398351, 425000, 0.35),
                                    ( 425001, 1e12, 0.396) ]

_cumsum_ca = numpy.cumsum([ (max_thresh - min_thresh) * rate for ( min_thresh, max_thresh, rate) in
                            _minmax_rate_ca_dict ])

_cumsum_fed = numpy.cumsum([ (max_thresh - min_thresh) * rate for ( min_thresh, max_thresh, rate) in
                             _minmax_rate_federal_joint_dict ])

def state_tax(total_income, mg = None, year = datetime.date.today().year):
    income_with_deduct = int( max(0, total_income - _standard_california_deduction) )
    min_threshs, max_threshs, marg_rates = zip(*_minmax_rate_ca_dict)
    for idx, min_thresh in enumerate(min_threshs):
        if income_with_deduct < min_thresh:
            break
    idx = idx - 1
    sttax = (income_with_deduct - min_threshs[idx]) * marg_rates[idx]
    if idx > 0:
        sttax = sttax + _cumsum_ca[idx-1]

    #
    ## now tricky part, start the clock on the property tax paid on the mortgage
    ## paid from 2 months after the first month of the date that the mortgage started
    state_tax = int(sttax * 100) * 0.01 - _standard_california_marriage_credit
    if mg is None:
        return state_tax
    else: # do an itemization
        date_start = mg.get_start_date()
        startyear = date_start.year
        if startyear < year:
            num_payments = 12
        else:
            num_payments = len(range( date_start.month, 13))
        
        state_tax -= mg.monthly_property_tax * num_payments
        return state_tax

"""
Just the federal tax without home deductions
"""
def federal_tax_standard(total_income, num_kids = 0):
    castate_total = state_tax( total_income ) + 0.01 * int( total_income )
    standard_deduct = max( 12200, castate_total )
    exemptions = _standard_federal_exemptions + 3950 * num_kids
    taxable_income = total_income - standard_deduct - exemptions
    return federal_baserate( taxable_income )

def federal_baserate(taxable_income):
    min_threshs, max_threshs, marg_rates = zip(*_minmax_rate_federal_joint_dict )
    for idx, min_thresh in enumerate(min_threshs):
        if taxable_income < min_thresh:
            break
    idx = idx - 1
    fedtax = ( taxable_income - min_threshs[idx]) * marg_rates[idx]
    if idx > 0:
        fedtax += _cumsum_fed[idx - 1]
    fedtax = int( fedtax * 100 ) * 0.01
    return fedtax

# I am including only the federal mortgage interest deduction
def federal_tax_with_home_deduct(total_income, num_kids = 0, mg = None,
                                 year = datetime.date.today().year ):
    base_fedtax = federal_tax_standard( total_income, num_kids = num_kids )
    if mg is None:
        return base_fedtax
    
    date_start = mg.get_start_date()
    if year < date_start.year:
        return base_fedtax

    if year >= date_start.year + 1:
        startnum = 12 * ( year - date_start.year ) - date_start.month + 2
        mortgint = mg.get_interest_amount_firstmonths(startnum, startnum + 12)
        nummonths = 12
    else:
        startnum = 1
        endnum = 12 - date_start.month + 1
        mortgint = mg.get_interest_amount_firstmonths(startnum, endnum)
        nummonths = endnum - startnum + 1

    #
    ## itemize the deduction
    castate_total = state_tax( total_income ) + 0.01 * int( total_income )
    proptax = mg.monthly_property_tax * nummonths
    exemptions = _standard_federal_exemptions + 3950 * num_kids
    totdeduct = proptax + mortgint + castate_total
    taxable_income = total_income - exemptions - totdeduct
    fedtax = federal_baserate( taxable_income )
    #
    ## return min of the two
    return min( base_fedtax, fedtax )

def income_after_tax_with_homededuct(total_income, num_kids = 0, mg = None,
                                     year = datetime.date.today().year ):
    if mg is None:
        fedtax = federal_tax_standard(total_income,
                                      num_kids = num_kids)
    else:
        date_start = mg.get_start_date()
        years = range( date_start.year, min( date_start.year, year) + 1 )
        fedtax = 0
        statetax = 0
        for yr in years:
            ft = federal_tax_with_home_deduct(total_income,
                                              num_kids = num_kids,
                                              mg = mg, year = yr)
            st = state_tax(total_income, mg = mg, year = yr )
            fedtax += ft
            statetax += st
        fedtax /= len(years)
        statetax /= len(years)
    return total_income - statetax - fedtax
