#!/usr/bin/env python
 
# make sure to go to the following page first
# https://selenium-python.readthedocs.org/getting-started.html
 
import os, find_catown_bbox_grid, time
import glob, tempfile, datetime
import gzip, pickle, shutil
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from optparse import OptionParser
from contextlib import contextmanager

def create_standard_ff_driver():    
    temp_dir = tempfile.mkdtemp()
    fp = webdriver.FirefoxProfile()
    fp.set_preference("browser.download.folderList",2)
    fp.set_preference("browser.download.manager.showWhenStarting", False)
    fp.set_preference("browser.download.dir", temp_dir)
    fp.set_preference("browser.helperApps.neverAsk.saveToDisk","text/csv")
    driver = webdriver.Firefox(firefox_profile=fp)
    driver.set_window_position(-2000,0)
    return temp_dir, driver

def create_standard_chrome_driver():
    temp_dir = tempfile.mkdtemp()
    driver = webdriver.Chrome(executable_path = 
                              '/usr/lib/chromium-browser/chromedriver')
    return temp_dir, driver

# borrowed from
# http://www.obeythetestinggoat.com/how-to-get-selenium-to-wait-for-page-load-after-a-click.html
def wait_for(condition_function):
    start_time = time.time()
    while time.time() < start_time + 3:
        if condition_function():
            return True
        else:
            time.sleep(0.1)
    raise Exception(
        'Timeout waiting for {}'.format(condition_function.__name__)
    )

@contextmanager
def wait_for_page_load(driver):
    old_page = driver.find_element_by_tag_name('html')
    yield
    def page_has_loaded():
        new_page = browser.find_element_by_tag_name('html')
        return new_page.id != old_page.id
    wait_for( page_has_loaded )

def create_data_for_town(town_name, outdir = os.getcwd(), toFile = True):
    dt = datetime.datetime.now()
    
    temp_dir, driver = create_standard_ff_driver()
    subURLs = find_catown_bbox_grid.find_urls_for_town_with_name(town_name)
    time0 = time.time()
    for mainURL in subURLs:
        driver.get(mainURL)
    
        # this is the table element in REDFIN house search
        # click on this element
        try:
            elem_table = driver.find_element_by_xpath('/html/body/div[1]/div[3]/div[3]/div[1]/div[11]/div[2]/div[1]')
            elem_table.click()
    
            # this is the element that downloads a CSV file
            elem_download = driver.find_element_by_link_text('DOWNLOAD')
            elem_download.click()
        except: # all possible reasons why the DOWNLOAD button could not be found??
            pass

    driver.quit()

    #
    ## now get all the files from that directory
    filenames = glob.glob(os.path.join(temp_dir, 'redfin*csv'))
    firstline = open(filenames[0], 'r').readlines()[0].replace('\n','').strip()
    all_lines = set(reduce(lambda x,y: x+y, [ open(filename, 'r').readlines()[1:] for 
                                              filename in filenames ] ))
    all_lines = [ firstline, ] + list(all_lines)
    shutil.rmtree( temp_dir )
    
    if toFile:
        filename = '_'.join(town_name.split()) + '_' + dt.strftime('%Y_%m_%d')
        pickle.dump(all_lines, gzip.open( os.path.join( outdir, '%s.pkl.gz' % filename), 'w'))
    print 'everything processed in %0.3f seconds.' % (time.time() - time0)
    return all_lines

if __name__=='__main__':
    parser = OptionParser()
    parser.add_option('--town', type=str, dest='town_name', action='store',
                      help = 'Name of the town to which to get Redfin year data. Default is Livermore.',
                      default = 'Livermore')
    parser.add_option('--outdir', type=str, dest='outdir', action='store',
                      help = 'Name of the directory to which to put this data. Default is %s.' %
                      os.getcwd())
    opts, args = parser.parse_args()
    create_data_for_town(opts.town_name, outdir = os.path.expanduser(opts.outdir))
