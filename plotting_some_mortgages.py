#!/usr/bin/env python

import numpy, pylab, os, sys, glob, datetime, titlecase
import mortgage_calc, income_taxrate_california, re
import pickle, gzip, selenium_redfin_demo
from optparse import OptionParser

def _get_town_name_date(filename):
    basefile = os.path.basename( filename )
    basefile = re.sub('\.pkl\.gz$', '', basefile).strip()
    townname = ' '.join( basefile.split('_')[:-3] )
    dtstring = ' '.join( basefile.split('_')[-3:] )
    date = datetime.datetime.strptime(dtstring, '%Y %m %d' ).date()
    return townname, date

def _calculate_monthly_payments(mg, percent_down, interest_rate, income = None):
    payment = mg.recalculate_payments(mg.total_value, percent_down = percent_down,
                                      interest_rate = interest_rate)
    return payment

def curve_monthly_payments(value_home, percent_down = 20.0, lpmi_on = True,
                           min_interest = 3.5, max_interest = 5.5, num_points = 201,
                           income = None):
    mg = mortgage_calc.Mortgage(total_value = value_home, percent_down = percent_down,
                                lpmi_on = lpmi_on)
    interest_rates = numpy.linspace( max_interest, min_interest, num_points )
    mon_pays = numpy.array([ _calculate_monthly_payments(mg, percent_down, rate, income = income) for rate in
                             interest_rates ])
    return interest_rates, mon_pays

"""
Draw lines showing 10% after-tax total monthly payments, 20% after-tax total monthly payments,
30% after-tax total monthly payments, and 50% after-tax total monthly payments, on salary

Draw out 
"""
def draw_home_calcs(all_lines, town, total_income = 130000, percent_down = 20.0,
                    actdate = datetime.datetime.now().date() ):
    all_tups = mortgage_calc.process_town_database(all_lines, town)
    sq_foots, dates_sold, prices_sold, addresses, latlngs = zip(*all_tups)
    prices_sold_sort = numpy.sort( prices_sold )
    leng = len(prices_sold_sort)
    #
    price_10per = prices_sold_sort[ int(leng * 0.10) ]
    price_25per = prices_sold_sort[ int(leng * 0.25) ]
    price_50per = prices_sold_sort[ int(leng * 0.50) ]
    price_75per = prices_sold_sort[ int(leng * 0.75) ]
    price_90per = prices_sold_sort[ int(leng * 0.90) ]
    
    fig = pylab.figure(figsize = (12, 9))
    ax = fig.add_subplot(111)    
            
    # scatter plot of house prices and interest rates for homes
    interest_rates = []
    monthly_rates = []
    monthly_after_taxes = []
    sizes = []
    colors = []
    my_cmap = pylab.get_cmap('jet')
    max_price = max(prices_sold)
    min_price = min(prices_sold)
    for date, square_foot, house_price in zip(dates_sold, sq_foots, prices_sold):
        mon_label = date.strftime('%B')
        year_label = date.year
        #interest_rate = mortgage_calc._30year_fha_dict[mon_label][year_label]
        interest_rate = mortgage_calc.get_mortgage_rate_fha(date)
        interest_rates.append(interest_rate)
        mg = mortgage_calc.Mortgage(total_value = house_price,
                                    interest_rate = interest_rate,
                                    percent_down = percent_down)
        monthly_rate = _calculate_monthly_payments(mg, percent_down, interest_rate,
                                                   income = total_income)
        monthly_rates.append(monthly_rate)
        #
        ## the after tax income on the home
        after_tax_income = income_taxrate_california.income_after_tax_with_homededuct(total_income,
                                                                                      mg = mg)
        monthly_after_tax = after_tax_income / 12.0
        monthly_after_taxes.append( monthly_after_tax )
        limit_monthly = 0.75 * monthly_after_tax
        #
        # size is based on square footage of home
        size = 20 * (square_foot / 1000)**2
        sizes.append(size)
        #
        # color by price of house
        colors.append( my_cmap( monthly_rate / limit_monthly ) )
    
    ax.scatter( interest_rates, monthly_rates, marker = 'o', s = sizes, c = colors)
    interest_rates_here = [ ir for ( ir, monthly_rate) in zip( interest_rates, monthly_rates ) if
                            monthly_rate <= 0.5 * monthly_after_tax ]
    max_interest_rate = max(interest_rates_here)
    min_interest_rate = min(interest_rates_here)
    monthly_after_tax = numpy.median( monthly_after_taxes )

    for color, pric, label, style in zip( [ 'red', 'green', 'blue', 'purple', 'black' ],
                                          [ price_10per, price_25per, price_50per, price_75per, price_90per ],
                                          [ '10%', '25%', 'median', '75%', '90%' ],
                                          ['--'] * 4):
        interest_rates, mon_pays = curve_monthly_payments(pric, income = total_income, percent_down = percent_down,
                                                          min_interest = 1.05 * min_interest_rate - 0.05 * max_interest_rate,
                                                          max_interest = 1.05 * max_interest_rate - 0.05 * min_interest_rate,
                                                          num_points = 151)
        ax.plot(interest_rates, mon_pays, style, linewidth = 4, color = color,
                alpha = 0.5, label = label)

    # draw these curves
    line_10per = [ 0.10 * monthly_after_tax ] * len(interest_rates)
    line_20per = [ 0.20 * monthly_after_tax ] * len(interest_rates)
    line_30per = [ 0.30 * monthly_after_tax ] * len(interest_rates)
    line_40per = [ 0.40 * monthly_after_tax ] * len(interest_rates)
    line_50per = [ 0.50 * monthly_after_tax ] * len(interest_rates)
    line_100per = [ monthly_after_tax ] * len(interest_rates)
    
    # lines showing 10-20% after tax, 20-30% after tax, 30-50% after tax, and 50+% after tax (death line)
    ax.fill_between(interest_rates, line_10per, line_20per, facecolor = 'green', alpha = 0.15,
                    interpolate = True, linewidth = 0)
    ax.fill_between(interest_rates, line_20per, line_30per, facecolor = 'blue', alpha = 0.15,
                    interpolate = True, linewidth = 0)
    ax.fill_between(interest_rates, line_30per, line_40per, facecolor = 'purple', alpha = 0.15,
                    interpolate = True, linewidth = 0)
    ax.fill_between(interest_rates, line_40per, line_50per, facecolor = 'magenta', alpha = 0.15,
                    interpolate = True, linewidth = 0)
    ax.fill_between(interest_rates, line_50per, line_100per, facecolor = 'red', alpha = 0.15,
                    interpolate = True, linewidth = 0)
    
    # sensible limits
    ax.set_ylim(top = 0.5 * monthly_after_tax, bottom = max( numpy.min(monthly_rates), 0.0))
    ax.set_xlim( (1.05 * min_interest_rate - 0.05 * max_interest_rate, 
                  1.05 * max_interest_rate - 0.05 * min_interest_rate ) )
    ax.invert_xaxis()
    ax.legend(loc='best')

    ax.set_xlabel('interest rate', fontsize = 18, fontweight = 'bold')
    ax.set_ylabel('total monthly payments', fontsize = 18, fontweight = 'bold')
    #
    supertitle = titlecase.titlecase( 'Affordability of %s Homes For the Past Year, From %s, For a $%dk/year Salary' %
                                      ( town, actdate.strftime('%m/%d/%y'), int( total_income / 1000.0) ) )
    ax.set_title('\nFrom '.join( supertitle.split(' From ') ),
                 fontsize = 18, fontweight = 'bold')
    ax.tick_params(labelsize = 14)
    fig.savefig( '%s_%s_homecalcs.pdf' % ( town, actdate.strftime('%Y_%m_%d') ),
                 bbox_inches = 'tight' )

def draw_scatter_home_prices(all_lines, town, total_income = 130000, percent_down = 20.0,
                             actdate = datetime.datetime.now().date() ):
    all_tups = mortgage_calc.process_town_database(all_lines, town)
    sq_foots, dates_sold, prices_sold, addresses, latlngs = zip(*all_tups)
    median_sqfoot = numpy.median(sq_foots)
    median_price = numpy.median(prices_sold)
    #
    my_cmap = pylab.get_cmap('jet')
    sizes = []
    colors = []
    monthly_rates_rel = []
    #
    for date, square_foot, house_price in zip(dates_sold, sq_foots, prices_sold):
        mon_label = date.strftime('%B')
        year_label = date.year
        #interest_rate = mortgage_calc._30year_fha_dict[mon_label][year_label]
        interest_rate = mortgage_calc.get_mortgage_rate_fha(date)
        mg = mortgage_calc.Mortgage(total_value = house_price,
                                    interest_rate = interest_rate,
                                    percent_down = percent_down, date = date)
        monthly_rate = _calculate_monthly_payments(mg, percent_down, interest_rate,
                                                   income = total_income)
        after_tax_income = income_taxrate_california.income_after_tax_with_homededuct(total_income, mg = mg)
        monthly_after_tax = after_tax_income / 12.0
        monthly_rates_rel.append(monthly_rate / monthly_after_tax )
        #
        # size is based on square footage of home
        size = 20 * (square_foot * 1.0 / 1000)**2
        sizes.append(size)
    
    max_rate = max(monthly_rates_rel)
    min_rate = min(monthly_rates_rel)    
    max_sqft_viz = sorted(sq_foots)[int( len(sq_foots) * 0.75 )]
    max_price_viz = sorted(prices_sold)[int( len(prices_sold) * 0.75 )]
    colors = [ my_cmap( rate / 0.75 ) for 
               rate in monthly_rates_rel ]
    #
    fig = pylab.figure(figsize = (12, 9))
    ax = fig.add_subplot(111)
    ax.scatter( sq_foots, numpy.array(prices_sold) / 1000, marker = 'o',
                c = colors, s = sizes, cmap = 'jet')
    ax.set_xlabel('house area (square feet)', fontsize = 18, fontweight = 'bold')
    ax.set_ylabel('sale price (in units of $1000)', fontsize = 18, fontweight = 'bold')

    ax.plot([ median_sqfoot, median_sqfoot ], [0.0, 1e9], '--', linewidth = 4, color = 'purple',
            alpha = 0.5)
    ax.plot([ 0.0, 1e9], [ median_price / 1000, median_price / 1000 ], '--', linewidth = 4, color = 'purple',
            alpha = 0.5)
    ax.set_xlim(0.9 * min(sq_foots), (2 * median_sqfoot - 0.9 * min(sq_foots) ) * 1.1 )
    ax.set_ylim(0.9 * min(prices_sold) / 1000, 1.1 * ( 2 * median_price - 0.9 * min(prices_sold) ) / 1000 )
    supertitle = titlecase.titlecase( 'Home Purchases For the Past Year From %s, in %s, For a $%dk/year Salary' %
                                      ( actdate.strftime('%m/%d/%y'), town, int( total_income / 1000.0) ) )
    ax.set_title('\nin '.join( supertitle.split(' in ') ),
                 fontsize = 18, fontweight = 'bold')
    for idx, rate in enumerate((0.75, 0.6, 0.40, 0.25, 0.1)):
        color = my_cmap( rate / 0.75 )
        ax.text(0.05, 0.95 - idx * 0.05, '%d%% of after-tax rate' % ( int(rate * 100) ),
                transform = ax.transAxes, horizontalalignment = 'left', verticalalignment = 'center',
                fontdict = { 'fontsize' : 14, 'fontweight' : 'bold', 'color' : color } )    
    fig.savefig( '%s_%s_scatter.pdf' % ( town, actdate.strftime('%Y_%m_%d') ),
                 bbox_inches = 'tight' )
    pylab.close()

if __name__=='__main__':
    parser = OptionParser()
    parser.add_option('--income', dest='income', type=int, action='store',
                      help = 'Before tax income in units of 1000s of dollars.')
    parser.add_option('--percentdown', dest='percentdown', type=int, action='store',
                      help = 'Percent down on loans. Default is 20 percent.',
                      default = 20)
    parser.add_option('--town', dest='town', type=str, action='store',
                      help = 'Name of the town to use.')
    parser.add_option('--filename', dest='filename', type=str, action='store',
                      help = 'If chosen, use the file name that has the data shown here')
    opts, args = parser.parse_args()
    if opts.filename is None:
        assert(all([ tok is not None for tok in ( opts.income, opts.town )]))
        all_lines = selenium_redfin_demo.create_data_for_town( opts.town,
                                                               toFile = False)
        draw_scatter_home_prices( all_lines, opts.town,
                                  total_income = opts.income * 1000,
                                  percent_down = opts.percentdown)
        draw_home_calcs( all_lines, opts.town,
                         total_income = opts.income * 1000,
                         percent_down = opts.percentdown)
    else:
        assert(all([ tok is not None for tok in ( opts.income, opts.filename ) ]))
        filename = os.path.expanduser( opts.filename )
        assert( os.path.isfile( filename ) )
        assert( os.path.basename( filename ).endswith('.pkl.gz'))
        all_lines = pickle.load( gzip.open( filename, 'r' ))
        town, date = _get_town_name_date( filename )
        draw_scatter_home_prices( all_lines, town,
                                  total_income = opts.income * 1000,
                                  percent_down = opts.percentdown,
                                  actdate = date )
        draw_home_calcs( all_lines, town,
                         total_income = opts.income * 1000,
                         percent_down = opts.percentdown,
                         actdate = date )
